<div class="wrapper hero" style="width: 100%; height: 100vh;"
data-vide-bg="video/hero_video" data-vide-options="position: 50% 50%"
>
  <div class="container">
    <div class="row">
      <div class="col-md-12 hero-message">
        <div class="text">
          <h1>We Live Disruption</h1>
          <!-- <p>Let’s face it. <strong>Successful digital transformations are rare</strong>. <strong>But they don’t have to be</strong>. We help daring executives lead effective digital transformations and <strong>create lasting change</strong>.</p> -->
          <p><strong>Let’s face it. Successful digital transformations are rare</strong>. But they don’t have to be. We help daring executives lead effective digital transformations and <strong>create lasting change</strong>.</p>          
        </div>
      </div>
    </div>
  </div>
</div>

<div class="wrapper intro">
  <div class="container">
    <div class="row">
      <div class="col-md-4 stat">
        <p>
          <span class="xl">76%</span> 
          <span class="lg">of Digital Stategies Fail</span>
        </p>
      </div>
      <div class="col-md-offset-1 col-md-7">
        <h2>XU Ensures Successful Digital Transformations</h2>
        <ul>
          <li><strong>Full spectrum offering:</strong> from mindset change to learning of practical skills</li>
          <li><strong>Effective implementation</strong> for sustainable digital transformation.</li>
          <li><strong>Innovative</strong> new business models, products and services.</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="wrapper schools" id="schools">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2><img class="logo" src="/images/X_schools.svg" alt="X.Schools"></h2>
        <h3>Successful digital transformations hinge on changing both hearts and minds</h3>
        <p>We offer and implement tailor-made programs with your needs in mind. <strong>X.Schools innovative and practical approach</strong> towards corporate education ensures the effective transfer of knowledge and skills required to empower your people to sustain change.</p>
      </div>
    </div>
  </div>
</div>
<div class="wrapper labs" id="labs">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2><img class="logo" src="/images/X_labs.svg" alt="X.Labs"></h2>
        <h3>Cutting-edge Technologies <br>Disruptive Methodologies</h3>
        <p>Innovation playgrounds that let you <strong>explore and rapidly test solutions</strong> with cutting-edge technologies and disruptive methods. Engage our expertise in 3 innovation topics: <strong>Big Data, Internet Of Things, Future of Retail</strong>, or co-create a <strong>tailor made innovation lab</strong> solely assembled to tackle your challenges. From planning to recruiting, we <strong>connect you with the smartest digital talents</strong> and help you <strong>strike strategic partnerships</strong> with startups.</p>
      </div>
      <div class="col-md-6">
        <img class="circle-image" src="/images/3d-printer.png" alt="Digital Labs">
      </div>
    </div>
  </div>
</div>
<div class="wrapper campus" id="campus">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h2>XU Campus</h2>
        <p>XU campus (in-development) have been designed to bring to life our vision of <strong>changing how people think and work</strong>. Over 8.000 sq.m solely for bringing together leading thinkers and makers to incubate the world changing ideas of tomorrow.</p>
      </div>
      <div class="col-md-offset-1 col-md-6">
        <img class="img-responsive" src="/images/X-campus.jpg" alt="X.Campus" >
      </div>
    </div>
  </div>
</div>
<div class="wrapper hr">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <hr>
      </div>
    </div>
  </div>
</div>
<div class="wrapper team" id="team">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2>XU’s Team of Experts</h2>
        <p>Our core team currently consists of over <strong>20 highly skilled experts &amp; leaders within education and digital</strong>. In addition, we also collaborate with top digital talents worldwide who are brought in based on their expertise to solve specific challenges.</p>
      </div>
      <div class="col-md-6">
        <ul>


          <!-- ROW -->
          <?php inc('team_member',[
            'file_name' => 'Alexander-Wolf.jpg',
            'name' => 'Alexander Wolf',
            'title' => 'Networking'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Allison-Antalek.jpg',
            'name' => 'Allison Antalek',
            'title' => 'Educational Content Development'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Dr-Christopher-Jahns.jpg',
            'name' => 'PD Dr. Christopher Jahns',
            'title' => 'Sales, Education, Management'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Henner-Rolvien.jpg',
            'name' => 'Henner Rolvien',
            'title' => 'Architect'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Ingo-Taurat.jpg',
            'name' => 'Ingo Taurat',
            'title' => 'Infrastructure'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Henning-Jakob.jpg',
            'name' => 'Henning Jakob',
            'title' => 'Technology and Tools'
          ]) ?>

          <!-- ROW -->
          <?php inc('team_member',[
            'file_name' => 'Jon-Amar.jpg',
            'name' => 'Jon Amar',
            'title' => 'Corporate Design'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Ahmet-Emre-Acar.jpg',
            'name' => 'Ahmet Emre Acar',
            'title' => 'Design Thinking'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Julie-Silverman.jpg',
            'name' => 'Julie Silverman',
            'title' => 'UX/IX Specialist'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Karim-Lankarany.jpg',
            'name' => 'Karim Lankarany',
            'title' => 'Ecommerce'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Katja-Horter.jpg',
            'name' => 'Katja Horter',
            'title' => 'Business & Partner Development'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Laura-Lüngen.jpg',
            'name' => 'Laura Lüngen',
            'title' => 'Social Media Marketing'
          ]) ?>          

          <!-- ROW -->
          <?php inc('team_member',[
            'file_name' => 'Carla-Mengual.jpg',
            'name' => 'Carla Mengual',
            'title' => 'Growth, SMM'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Marcus-Adam.jpg',
            'name' => 'Marcus Adam',
            'title' => 'Events'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Max-Kotin.jpg',
            'name' => 'Maxim Kotin',
            'title' => 'Editorial & Publishing'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Nicole-Gaiziunas-Jahns.jpg',
            'name' => 'Nicole Gaiziunas Jahns',
            'title' => 'Strategy, Education & Management'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Charlotte-Geyer.jpg',
            'name' => 'Charlotte Geyer',
            'title' => 'Sales & Education'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Olga-Steidl.jpg',
            'name' => 'Olga Steidl',
            'title' => 'Business Development & Digital Community'
          ]) ?>

          <!-- ROW -->
          <?php inc('team_member',[
            'file_name' => 'Stephan-Balzer.jpg',
            'name' => 'Stephan Balzer',
            'title' => 'Trainer, Network, Events, TEDx'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Dr-Tillman-Bronner.jpg',
            'name' => 'Dr. Tillman Bronner',
            'title' => 'Finance'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Viktoriya-Gorodetska.jpg',
            'name' => 'Viktoriya Gorodetska',
            'title' => 'Organisation'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Wencke-Martin.jpg',
            'name' => 'Wencke Martin',
            'title' => 'Trainer Network'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Johannes-Ballestrem.jpg',
            'name' => 'Johannes Ballestrem',
            'title' => 'Special Projects'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Merlin-Ortner.jpg',
            'name' => 'Merlin Ortner',
            'title' => 'Campus Concept'
          ]) ?>

          <!-- ROW -->
          <?php inc('team_member',[
            'file_name' => 'Dr-Uwe-Eisermann.jpg',
            'name' => 'Dr. Uwe Eisermann',
            'title' => 'University'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Aljoscha-Kaplan.jpg',
            'name' => 'Aljoscha Kaplan',
            'title' => 'Coding Academy'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'Thilo-Zieschang.jpg',
            'name' => 'Thilo Zieschang',
            'title' => 'Cyber Security'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'galvanize.jpg',
            'name' => 'Galvanize',
            'title' => 'Developer School'
          ]) ?>
          <?php inc('team_member',[
            'file_name' => 'google.jpg',
            'name' => 'Google',
            'title' => 'Assessment Tool'
          ]) ?>

        </ul>      
      </div>
    </div>
  </div>
</div>
<div class="wrapper cta">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-6 heading">
            <h3>Curious About Your Digital Capability?</h3>
          </div>
          <div class="col-md-6">
            <a href="#" class="btn"><strong>Take The Assessment</strong></a>
            <p><strong>Developed in Partnership with <img src="/images/google.jpg"/></strong></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="wrapper footer">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p>+49 (030) 959999990&nbsp; &nbsp; &nbsp; &nbsp;<a href="mailto:team@url.com">team@url.com</a></p>
        <p><small>&copy; 2016 X-WORK EXponential Education &amp; CoWORKing GmbH</small></p>
      </div>
    </div>
  </div>
</div>