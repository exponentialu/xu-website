<li 
  class="team-member" 
  style="background-image:url(/images/team/<?= $file_name ?>);" 
  title="
    <strong><?= $name ?></strong> <br> 
    <?= $title ?>
    "
  data-toggle="tooltip" 
  data-placement="bottom" 
  data-html="true">
</li>